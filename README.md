## CT00CM01-3001 Software Development Skills: Full-Stack project

# Issue Tracking app
By Raoul Ölander

## How to run this project

### Requirements:
- Latest stable NodeJS and NPM
- Latest Angular cli installed globally
- MongoDB running as a service

### Running the app
1. Open a terminal and go to a folder where you wish to clone the repository
2. Clone the repository: `git clone https://olandra@bitbucket.org/olandra/lut-full-stack.git`
3. Move into the project folder: `cd lut-full-stack/Project/issuetracker`
4. Run: `npm install` 
5. With MongoDB running, run the back end: `npm run dev`
6. Open another terminal and go to the Angular source folder: `cd lut-full-stack/Project/issuetracker/angular-src`
7. Run: `npm install` 
8. Run the Angular front-end: `ng serve`
9. Open a web browser and go to URL: `http://localhost:4200`

First user is created as admin
Users after the first are created as users