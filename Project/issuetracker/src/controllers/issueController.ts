import { Request, Response } from "express";
import { IIssue, Issue } from "../models/issue";

export class IssueController {

    /**
     * Get all issues
     * 
     * @param req HTTP Request
     * @param res HTTP Response
     * 
     * @returns JSON array of issues as a HTTP response
     */
    public async getIssues(req: Request, res: Response): Promise<void> {
        const issues = await Issue.find();
        res.json({ issues });
    }

    /**
     * Get issue by id
     * 
     * @param req HTTP Request with the id
     * @param res HTTP Response
     * 
     * @returns JSON of the issue as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async getIssue(req: Request, res: Response): Promise<void> {
        const issue = await Issue.findOne({ id: req.params.id });
        if (issue === null) {
            res.sendStatus(404);
        } else {
            res.json(issue);
        }
    }

    /**
     * Create a new issue
     * 
     * @param req HTTP Request with the issue data
     * @param res HTTP Response
     * 
     * @returns HTTP status 201 and JSON if the issue as a HTTP response
     * @returns HTTP status 500 if the issue couldn't be added to the database
     * @returns HTTP status 422 shouldn't happen, but is there in the case someone finds a way to override an existing issue
     */
    public async createIssue(req: Request, res: Response): Promise<void> {
        const newIssue: IIssue = new Issue(req.body);
        const issues = await Issue.find();
        let maxId: string = "0";
        if(issues.length !== 0){
            maxId = await Issue.find().sort({id: -1}).limit(1).then(id => id[0].id);
        }
        newIssue.id = String(parseInt(maxId) + 1);
        const issue = await Issue.findOne({ id: req.body.id });         // Change to get from the database
        if (issue === null) {
            const result = await newIssue.save();
            if (result === null) {
                res.sendStatus(500);
            } else {
                res.status(201).json({ status: 201, data: result });
            }

        } else {
            res.sendStatus(422);
        }
    }

    /**
     * Patch update issue by id
     * 
     * @param req HTTP Request with id and req.body including the data to be updated
     * @param res HTTP Response
     * 
     * @returns JSON of the updated issue as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async updateIssue(req: Request, res: Response): Promise<void> {
        const issue = await Issue.findOneAndUpdate({ id: req.params.id }, req.body);
        if (issue === null) {
            res.sendStatus(404);
        } else {
            const updatedIssue = { id: req.params.id, ...req.body };
            res.json({ status: res.status, data: updatedIssue });
        }
    }

    /**
     * Get issue by id
     * 
     * @param req HTTP Request with id
     * @param res HTTP Response
     * 
     * @returns JSON of the deleted issue as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async deleteIssue(req: Request, res: Response): Promise<void> {
        const issue = await Issue.findOneAndDelete({ id: req.params.id });
        if (issue === null) {
            res.sendStatus(404);
        } else {
            res.json({ response: "Issue deleted Successfully" });
        }
    }
}