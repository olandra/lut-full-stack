import bcrypt from "bcrypt-nodejs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import passport from "passport";
import "../auth/passportHandler";
import {  User } from "../models/user";
import { JWT_SECRET } from "../util/secrets";

export class UserController {

    /**
     * Get all users
     * 
     * @param req HTTP Request
     * @param res HTTP Response
     * 
     * @returns JSON array of users as a HTTP response
     */
    public async getUsers(req: Request, res: Response): Promise<void> {
        const users = await User.find();
        res.json({ users });
    }

    /**
     * Get user by id
     * 
     * @param req HTTP Request with id
     * @param res HTTP Response
     * 
     * @returns JSON of the user as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async getUser(req: Request, res: Response): Promise<void> {
        const user = await User.findOne({ id: req.params.id });
        if (user === null) {
            res.sendStatus(404);
        } else {
            res.json(user);
        }
    }

    /**
     * Patch update user by id
     * 
     * @param req HTTP Request with id and req.body including the data to be updated
     * @param res HTTP Response
     * 
     * @returns JSON of the updated user as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async updateUser(req: Request, res: Response): Promise<void> {
        const user = await User.findOneAndUpdate({ id: req.params.id }, req.body);
        if (user === null) {
            res.sendStatus(404);
        } else {
            const updatedUser = { id: req.params.id, ...req.body };
            res.json({ status: res.status, data: updatedUser });
        }
    }

    /**
     * Delete user by id
     * 
     * @param req HTTP Request with id
     * @param res HTTP Response
     * 
     * @returns JSON of the deleted user as a HTTP response
     * @returns HTTP status 404 if not found
     */
    public async deleteUser(req: Request, res: Response): Promise<void> {
        const user = await User.findOneAndDelete({ id: req.params.id });
        if (User === null) {
            res.sendStatus(404);
        } else {
            res.json({ response: "User deleted Successfully" });
        }
    }

    /**
     * Register a new user
     * 
     * First user is created as an admin, the following as users
     * 
     * @param req HTTP Request with req.body including the data of the new user
     * @param res HTTP Response
     * 
     * @returns JSON of the updated user as a HTTP response
     * @returns HTTP status 403 if username already exists
     */
    public async registerUser(req: Request, res: Response): Promise<void> {
        const users = await User.find();

        // If user already exists
        const user = await User.findOne({username: req.body.username})
        if(user) {
            res.sendStatus(403);
        } else {
            const hashedPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
            let maxId: string = "0";
            if(users.length !== 0){
                maxId = await User.find().sort({id: -1}).limit(1).then(id => id[0].id);
            }
        
            await User.create({
                id: String(parseInt(maxId) + 1),
                name: req.body.name,
                email: req.body.email,
                username: req.body.username,
                password: hashedPassword,
                role: users.length === 0 ? "admin" : "user"
        
            });
        
            const token = jwt.sign({ username: req.body.username }, JWT_SECRET);
            res.status(200).send({ token: token });
        }
    }

    /**
     * Authenticates user and provides an access token via local strategy
     * 
     * @param req HTTP Request with username and password
     * @param res HTTP Response
     * @param next NextFunction
     * 
     * @returns Modified user with just the required data and the access token
     * @returns HTTP status 401 if authorization fails
     */
    public authenticateUser(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("local", function (err, user, info) {
            // The traditional way with a callback - Could implement async/await too
            if (err) return next(err);
            if (!user) {
                return res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                const token = jwt.sign({ username: user.username }, JWT_SECRET);
                let response = {
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    role: user.role,
                    token: token
                }
                res.status(200).send(response);
            }
        })(req, res, next);
      }
}