import { NextFunction, Request, Response } from "express";
import passport from "passport";
import "../auth/passportHandler";

export class AuthController {

    /**
     * Authorize an user and provide access token
     * 
     * @param req HTTP Request including a bearer token as an authentication header
     * @param res HTTP Response
     * @param next NextFunction
     * 
     * @returns NextFunction if authentication succeeds
     * @returns HTTP Status 401 if authentication fails or user is not found
     */
    public authenticateJWT(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("jwt", function (err, user, info) {
        if (err) {
            console.log(err);
            return res.status(401).json({ status: "error", code: "unauthorized" });
        }
        if (!user) {
            return res.status(401).json({ status: "error", code: "unauthorized" });
        } else {
            return next();
        }
        })(req, res, next);
    }

    /**
     * Authorize an user and provide access token and checks for role greater than "user"
     * 
     * @param req HTTP Request including a bearer token as an authentication header
     * @param res HTTP Response
     * @param next NextFunction
     * 
     * @returns NextFunction if authentication succeeds
     * @returns HTTP Status 401 if authentication fails, user is not found or user role is "user"
     */
    public authorizeJWT(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("jwt", function (err, user, info) {
            if (err) {
                console.log(err);
                return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            if (!user) {
                return res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                if(user.role !== "user")
                    return next();
                else
                    return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            })(req, res, next);
    }

    /**
     * Authorize an user and provide access token and checks for role of "admin"
     * 
     * @param req HTTP Request including a bearer token as an authentication header
     * @param res HTTP Response
     * @param next NextFunction
     * 
     * @returns NextFunction if authentication succeeds
     * @returns HTTP Status 401 if authentication fails, user is not found or user role is not "admin"
     */
    public authorizeAdminJWT(req: Request, res: Response, next: NextFunction) {
        passport.authenticate("jwt", function (err, user, info) {
            if (err) {
                console.log(err);
                return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            if (!user) {
                return res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                if(user.role === "admin")
                    return next();
                else
                    return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            })(req, res, next);
    }
}