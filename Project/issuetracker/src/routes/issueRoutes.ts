import { Router } from "express";
import { AuthController } from "../controllers/authController";
import { IssueController } from "../controllers/issueController";



export class IssueRoutes {

    public router: Router;
    public issueController: IssueController = new IssueController();
    public authController: AuthController = new AuthController();

    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Route guards:
     * authenticateJWT - User must be logged in
     * authorizeJWT - User must be logged in and have a role above common user (employee or admin)
     */

    routes() {
        this.router.get("/", this.authController.authenticateJWT, this.issueController.getIssues);
        this.router.get("/:id", this.authController.authenticateJWT, this.issueController.getIssue);
        this.router.post("/", this.authController.authenticateJWT, this.issueController.createIssue);
        this.router.patch("/:id", this.authController.authorizeJWT, this.issueController.updateIssue);
        this.router.delete("/:id", this.authController.authorizeJWT, this.issueController.deleteIssue);
    }
}