import { Router } from "express";
import { AuthController } from "../controllers/authController";
import { UserController } from "../controllers/userController";


export class UserRoutes {

    public router: Router;
    public userController: UserController = new UserController();
    public authController: AuthController = new AuthController();

    constructor() {
        this.router = Router();
        this.routes();
    }

    /**
     * Route guards:
     * authenticateJWT - User must be logged in
     * authorizeJWT - User must be logged in and have a role above common user (employee or admin)
     * authorizeAdminJWT - User must be logged in and have a role of admin
     */

    routes() {
        this.router.get("/", this.authController.authorizeJWT, this.userController.getUsers);
        this.router.get("/:id", this.authController.authenticateJWT, this.userController.getUser);
        this.router.patch("/:id", this.authController.authorizeAdminJWT, this.userController.updateUser);
        this.router.delete("/:id", this.authController.authorizeAdminJWT, this.userController.deleteUser);
        this.router.post("/register", this.userController.registerUser);
        this.router.post("/login", this.userController.authenticateUser);
    }
}