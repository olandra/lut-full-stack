import { Document, Schema, Model, model, Error } from "mongoose";

export interface IIssue extends Document {
    id: string;
    date: String;
    title: String;
    issue: String;
    status: String;
    handlerId: String;
    author: String;
    email: String;
}

export const issueSchema = new Schema({
    id: {
        type: String, required: true,
        unique: true
    },
    date: String,
    title: String,
    issue: String,
    status: String,
    handlerId: String,
    author: String,
    email: String
});

export const Issue: Model<IIssue> = model<IIssue>("Issue", issueSchema);