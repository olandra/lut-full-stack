import { Document, Schema, Model, model, Error } from "mongoose";
import bcrypt from "bcrypt-nodejs";

export interface IUser extends Document {
    id: string;
    name: String;
    email: String;
    username: string;
    password: string;
    role: String;
}

export const userSchema = new Schema({
    id: {
        type: String, required: true,
        unique: true
    },
    name: String,
    email: String,
    username: String,
    password: String,
    role: String
});

userSchema.methods.comparePassword = function (candidatePassword: string, callback: any) {
    bcrypt.compare(candidatePassword, this.password, (err: Error, isMatch: boolean) => {
        callback(err, isMatch);
    });
};

export const User: Model<IUser> = model<IUser>("User", userSchema);