import passport from "passport";
import passportLocal from "passport-local";
import passportJwt from "passport-jwt";
import { User } from "../models/user";
import { JWT_SECRET } from "../util/secrets";


const LocalStrategy = passportLocal.Strategy;
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

/**
 * Passport local strategy to validate user login
 */
passport.use(new LocalStrategy({ usernameField: "username" }, (username: string, password: string, done) => {
    User.findOne({ username: username }, (err: any, user: any) => {
        if (err) { return done(err); }
        if (!user) {
            return done(undefined, false, { message: `username ${username} not found.` });
        }
        user.comparePassword(password, (err: Error, isMatch: boolean) => {
            if (err) { return done(err); }
            if (isMatch) {
                return done(undefined, user);
            }
            return done(undefined, false, { message: "Invalid username or password." });
        });
    });
}));

/** 
 * Passport JWT strategy to authenticate bearer access token
 */
passport.use(new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: JWT_SECRET
    }, function (jwtToken, done) {
    User.findOne({ username: jwtToken.username }, function (err: any, user: any) {
        if (err) { return done(err, false); }
        if (user) {
            return done(undefined, user , jwtToken);
        } else {
            return done(undefined, false);
        }
    });
}));