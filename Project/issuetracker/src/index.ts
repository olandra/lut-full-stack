import express from "express";
import mongoose from "mongoose";
import morgan from "morgan";
import compression from "compression";
import cors from "cors";
import bodyParser from "express";
import { MONGODB_URI } from "./util/secrets";

import { IssueRoutes } from "./routes/issueRoutes";
import { UserRoutes } from "./routes/userRoutes";

class Server {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
        this.mongo();
    }

    public routes(): void {
        this.app.use("/api/issues", new IssueRoutes().router);
        this.app.use("/api/users", new UserRoutes().router);
    }

    public config(): void {
        this.app.set("PORT", process.env.PORT || 3000);
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(compression());
        this.app.use(morgan("tiny"));
        this.app.use(express.static("public"));
        this.app.use(cors());
        this.app.use(bodyParser());
    }

    private mongo() {
        const connection = mongoose.connection;
        connection.on("connected", () => {
        console.log("Mongo Connection Established");
        });
        connection.on("reconnected", () => {
        console.log("Mongo Connection Reestablished");
        });
        connection.on("disconnected", () => {
        console.log("Mongo Connection Disconnected");
        console.log("Trying to reconnect to Mongo ...");
        setTimeout(() => {
            mongoose.connect(MONGODB_URI, {
                keepAlive: true,
            socketTimeoutMS: 3000, connectTimeoutMS: 3000
            });
        }, 3000);
        });
        connection.on("close", () => {
        console.log("Mongo Connection Closed");
        });
        connection.on("error", (error: Error) => {
        console.log("Mongo Connection ERROR: " + error);
        });
    
        const run = async () => {
        await mongoose.connect(MONGODB_URI, {
            keepAlive: true
        });
        };
        run().catch(error => console.error(error));
      }

    public start(): void {
        this.app.listen(this.app.get("PORT"), () => {
            console.log("Server is running on port", this.app.get("PORT"));
        });
    }
}

const server = new Server();

server.start();