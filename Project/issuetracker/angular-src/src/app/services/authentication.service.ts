import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config } from '../config';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  private config = new Config();

  constructor(private http: HttpClient) {
    let localStorageUser = localStorage.getItem('currentUser');
    this.currentUserSubject = new BehaviorSubject<any>(localStorageUser !== null ? JSON.parse(localStorageUser) : null);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  /**
   * Login request to the back end and stores it to the LocalStorage
   * 
   * @param username Username: string
   * @param password Password: string
   * @returns Required user data object (id, name, email, role and access token)
   */
  login(username: string, password: string) {
    let user = {
      username: username,
      password: password
    }
    return this.http.post<any>(this.config.loginURL, user)
      .pipe(map(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;
      }))
  }

  /**
   * Logs user out by clearing the LocalStorage
   */
  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
