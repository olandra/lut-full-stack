import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private config = new Config();

  constructor(private http: HttpClient) { }

  /**
   * Get all users request to the back end
   * 
   * @returns JSON array of all users
   */
  getUsers() {
    return this.http.get<any>(this.config.usersURL);
  }

  /**
   * Get user by id request to the back end
   * 
   * @param id number 
   * @returns JSON of a user
   */
  getUserById(id: number) {
    return this.http.get<any>(this.config.usersURL + '/' + id)
  }

  /**
   * Create user request to the back end
   * 
   * @param user User object
   */
  register(user: any) {
    return this.http.post(this.config.registerURL, user);
  }

  /**
   * Update user request to the back end
   * 
   * @param id User id
   * @param role New role of the user
   */
  updateUserRole(id: number, role: string) {
    let updatedRole = {
      role: role
    }
    return this.http.patch(this.config.usersURL + '/' + id.toString(), updatedRole)
  }

  /**
   * Delete user by id request to the back end
   * 
   * @param id User id
   */
  deleteUserById(id: number) {
    return this.http.delete(`${this.config.usersURL}/${id}`)
  }
}
