import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Config } from '../config';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  private config = new Config();

  constructor(private http: HttpClient) { }

  /**
   * Create issue request to the back end
   * 
   * @param issue Issue object
   */
  newIssue(issue: any) {
    return this.http.post(this.config.issuesURL, issue);
  }

  /**
   * Update issue status request to the back end
   * 
   * @param id Issue id
   * @param userId User id
   * @param status Tracking role: string
   */
  updateIssueStatus(id: number, userId: number, status: string) {
    let issueStatus = {
      status: status,
      handlerId: status !== "untracked" ? userId : 0
    }
    return this.http.patch(this.config.issuesURL + '/' + id.toString(), issueStatus)
  }
}
