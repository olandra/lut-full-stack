import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Config } from 'src/app/config';
import { first } from 'rxjs/operators';
import { IssueService } from 'src/app/services/issue.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-new-issue',
  templateUrl: './new-issue.component.html',
  styleUrls: ['./new-issue.component.css']
})
export class NewIssueComponent implements OnInit {
  newIssueForm!: FormGroup;
  loading = false;
  submitted = false;
  private config = new Config();

  constructor(
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private issueService: IssueService
    ) { }

  /**
   * Create the form on init
   */
  ngOnInit(): void {
    this.newIssueForm = this.formBuilder.group({
      issueTitle: ['', Validators.required],
      issue: ['', Validators.required]
    })
  }

  get f() { return this.newIssueForm.controls; }

  /**
   * Create new issue request to the issue service
   */
  onSubmit() {
    this.submitted = true;

    this.alertService.clear();

    if(this.newIssueForm.invalid) {
      return;
    }

    this.loading = true;
    
    const userString = localStorage.getItem('currentUser');
    const user = userString ? JSON.parse(userString) : null;

    const date: Date = new Date();
    let timeStamp: string = '';
    timeStamp += date.getDay() + '.';
    timeStamp += (date.getMonth() + 1) + '.';
    timeStamp += date.getFullYear() + ' ';
    timeStamp += date.getHours() + ':';
    timeStamp += date.getMinutes() + ':';
    timeStamp += date.getSeconds();

    const issue = {
      date: timeStamp,
      title: this.newIssueForm.value.issueTitle,
      issue: this.newIssueForm.value.issue,
      status: 'untracked',
      handlerId: 0,
      author: user.name,
      email: user.email
    }

    console.log(issue)

    this.issueService.newIssue(issue)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('New issue created', true);
          window.location.reload();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      )
  }
}
