import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {
  currentUser: any = null;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
    ) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

  /**
   * Logout request to the authentication service
   */
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
