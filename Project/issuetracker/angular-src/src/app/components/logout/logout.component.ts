import { Component, OnInit } from '@angular/core';
import { LogoutService } from './logout.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  currentUser: any = null;

  constructor(
    private logoutService: LogoutService
    ) {}

  ngOnInit(): void {
    this.logout();
  }

  logout() {
    this.logoutService.logout();
  }
}
