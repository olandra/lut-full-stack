import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Config } from 'src/app/config';
import { IssueService } from 'src/app/services/issue.service';
import { UserService } from 'src/app/services/user.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-show-issues',
  templateUrl: './show-issues.component.html',
  styleUrls: ['./show-issues.component.css']
})
export class ShowIssuesComponent implements OnInit {
  
  issues: any = [];
  user: any;
  private config = new Config()

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private issueService: IssueService,
    private alertService: AlertService
    ) {
      let tempUser = localStorage.getItem('currentUser');
      this.user = tempUser !== null ? JSON.parse(tempUser) : null;
    }

  /**
   * Get all issues from the user service on init and hide loader when done
   */
  ngOnInit(): void {
    this.http.get(this.config.issuesURL)
      .subscribe(Response => {
  
        // If response comes hideLoader() function is called
        // to hide that loader
        if(Response) { 
          this.hideLoader();
        }
        this.issues = {...Response};
        for(let i = 0; i < this.issues.issues.length; i++) {
          if(this.issues.issues[i].handlerId !== '0') {
            this.userService.getUserById(this.issues.issues[i].handlerId)
              .pipe(first())
              .subscribe(Response => {
                if(Response) {
                  this.issues.issues[i].handler = Response
                }
              })
          }
        }
      });
  }

  /**
   * Hide the spinning loading text
   */
  hideLoader() {
    const element = window.document.getElementById('loading');
    if(element !== null) {
      element.style.display = 'none';
    }
  }

  /**
   * Update issue status request to the issue service
   * 
   * @param id Issue id: number
   * @param status Issue status: string
   */
  updateIssueStatus(id: number, status: string) {
    this.issueService.updateIssueStatus(id, this.user.id, status)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success(`New status: ${status}`, false)
        },
        error => {
          this.alertService.error(`${error}`)
        }
      )
  }

  /**
   * Find the issue index in the issues array by issue id
   * 
   * @param id Issue id in the database: number
   * @returns Issue index in the array of issue objects: number or null
   */
  findIssueFromIssuesArray(id: number) {
    for(let i = 0; i < this.issues.issues.length; i++) {
      if(this.issues.issues[i].id === id.toString()) {
        return i;
      }
    }
    return null;
  }

  /**
   * Untrack issue event listener
   * 
   * Changes the tracking status of an issue in database and in issues array
   * 
   * @param event ClickEvent
   */
  untrack(event: any) {
    let tempId = this.findIssueFromIssuesArray(event.target.id)
    if(tempId !== null) {
      this.issues.issues[tempId].handler = null
      this.issues.issues[tempId].status = "untracked"
      this.updateIssueStatus(event.target.id, "untracked")
    }
  }

  /**
   * Track issue event listener
   * 
   * Changes the tracking status of an issue in database and in issues array
   * 
   * @param event ClickEvent
   */
  track(event: any) {
    let tempId = this.findIssueFromIssuesArray(event.target.id)
    if(tempId !== null) {
      this.issues.issues[tempId].handler = this.user
      this.issues.issues[tempId].status = "tracked"
      this.updateIssueStatus(event.target.id, "tracked")
    }
  }

  /**
   * Close issue event listener
   * 
   * Changes the tracking status of an issue in database and in issues array
   * 
   * @param event ClickEvent
   */
  close(event: any) {
    let tempId = this.findIssueFromIssuesArray(event.target.id)
    if(tempId !== null) {
      this.issues.issues[tempId].handler = this.user
      this.issues.issues[tempId].status = "closed"
      this.updateIssueStatus(event.target.id, "closed")
    }
  }

  /**
   * Ignore issue event listener
   * 
   * Changes the tracking status of an issue in database and in issues array
   * 
   * @param event ClickEvent
   */
  ignore(event: any) {
    let tempId = this.findIssueFromIssuesArray(event.target.id)
    if(tempId !== null) {
      this.issues.issues[tempId].handler = this.user
      this.issues.issues[tempId].status = "ignored"
      this.updateIssueStatus(event.target.id, "ignored")
    }
  }
}
