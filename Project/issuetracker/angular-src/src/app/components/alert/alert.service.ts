import { Injectable } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  private subject = new Subject<any>();
  private keepAfterRouteChange = false;

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if(event instanceof NavigationStart) {
        if(this.keepAfterRouteChange) {
          this.keepAfterRouteChange = false;
        } else {
          this.clear();
        }
      }
    })
  }

  /**
   * Get alert data
   * 
   * @returns Subject: Observable
   */
  getAlert(): Observable<any> {
    return this.subject.asObservable();
  }

  /**
   * Create success alert
   * 
   * @param message Success message: string
   * @param keepAfterRouteChange Keep after route change: boolean
   */
  success(message: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.subject.next({ type: 'success', text: message });
  }

  /**
   * Create error alert
   * 
   * @param message Success message: string
   * @param keepAfterRouteChange Keep after route change: boolean
   */
  error(message: string, keepAfterRouteChange = false) {
    this.keepAfterRouteChange = keepAfterRouteChange;
    this.subject.next({ type: 'error', text: message });
  }

  /**
   * Clear current alert
   */
  clear() {
    this.subject.next();
  }
}
