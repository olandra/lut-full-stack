import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from './alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  private subscription!: Subscription;
  message: any;

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.subscription = this.alertService.getAlert()
      .subscribe(message => {
        switch (message && message.type) {
          case 'success':
            message.cssClass = 'alert alert-success alert-sticky';
            break;
          case 'error':
            message.cssClass = 'alert alert-danger alert-sticky';
            break;
        }
        this.message = message;
        setTimeout(() => {
          if(message) {
            message.cssClass = '';
          }
          this.message = null;
        }, 4000);
      })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
