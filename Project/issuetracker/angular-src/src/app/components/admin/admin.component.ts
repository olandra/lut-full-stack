import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Config } from 'src/app/config';
import { UserService } from 'src/app/services/user.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  users: any = [];
  user: any;
  private config = new Config();

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private alertService: AlertService
  ) {
    let tempUser = localStorage.getItem('currentUser');
    this.user = tempUser !== null ? JSON.parse(tempUser) : null;
  }

  /**
   * Get users array on init and hide loader when done
   */
  ngOnInit(): void {
    this.http.get(this.config.usersURL)
      .subscribe(Response => {
        if(Response) {
          this.hideLoader();
        }
        this.users = {...Response}
      })
  }

  /**
   * Hide the spinning loading text
   */
  hideLoader() {
    const element = window.document.getElementById('loading');
    if(element !== null) {
      element.style.display = 'none';
    }
  }

  /**
   * Creates update user status by id request to user service
   * 
   * @param id User id: number
   * @param role New user role: string
   */
  updateUserRole(id: number, role: string) {
    this.userService.updateUserRole(id, role)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success(`New role: ${role}`, false)
        },
        error => {
          this.alertService.error(`${error}`)
        }
      )
  }

  /**
   * Creates delete user by id request to user service
   * 
   * @param id User id: number
   */
  deleteUserById(id: number) {
    this.userService.deleteUserById(id)
      .pipe(first())
      .subscribe()
  }

  /**
   * Find the user index in the users array by issue id
   * 
   * @param id User id in the database: number
   * @returns User index in the array of users objects: number or null
   */
  findUserFromLocalUsers(id: number) {
    for(let i = 0; i < this.users.users.length; i++) {
      if(this.users.users[i].id === id.toString()) {
        return i;
      }
    }
    return null;
  }

  /**
   * Make admin event listener
   * 
   * Changes the user role to admin in database and in userss array
   * 
   * @param event ClickEvent
   */
  makeAdmin(event: any) {
    let tempId = this.findUserFromLocalUsers(event.target.id)
    if(tempId !== null) {
      this.users.users[tempId].role = 'admin'
      this.updateUserRole(event.target.id, 'admin')
    }
  }

  /**
   * Make employee event listener
   * 
   * Changes the user role to employee in database and in userss array
   * 
   * @param event ClickEvent
   */
  makeEmployee(event: any) {
    let tempId = this.findUserFromLocalUsers(event.target.id)
    if(tempId !== null) {
      this.users.users[tempId].role = 'employee'
      this.updateUserRole(event.target.id, 'employee')
    }
  }

  /**
   * Make user event listener
   * 
   * Changes the user role to user in database and in userss array
   * 
   * @param event ClickEvent
   */
  makeUser(event: any) {
    let tempId = this.findUserFromLocalUsers(event.target.id)
    if(tempId !== null) {
      this.users.users[tempId].role = 'user'
      this.updateUserRole(event.target.id, 'user')
    }
  }
  
  /**
   * Delete user event listener
   * 
   * Deletes the user from the database and from the users array
   * 
   * @param event ClickEvent
   */
  deleteUser(event: any) {
    let tempId = this.findUserFromLocalUsers(event.target.id)
    if(tempId !== null) {
      this.alertService.success(`User ${this.users.users[tempId].name} deleted`, false)
      delete this.users.users[tempId]
      this.deleteUserById(event.target.id)
    } else {
      this.alertService.error(`User not found`, false)
    }
  }
}
