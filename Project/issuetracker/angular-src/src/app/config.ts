/**
 * Config file for localhost testing with the backend
 */
export class Config {
    issuesURL: string = 'http://localhost:3000/api/issues';
    usersURL: string = 'http://localhost:3000/api/users';
    registerURL: string = 'http://localhost:3000/api/users/register';
    loginURL: string = 'http://localhost:3000/api/users/login';
}